$(document).ready(function() {
	$('.carousel').carousel({
		interval: 2500
	})

	$("#carousel-prev").click(function() {
		$('.carousel').carousel('prev');
	});

	$("#carousel-next").click(function() {
		$('.carousel').carousel('next');
	});

	$(".alert").alert()
});